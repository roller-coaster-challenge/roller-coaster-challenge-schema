const fs = require('fs');
const Ajv = require('ajv');
const ajv = new Ajv({allErrors: true});

const schema = JSON.parse(fs.readFileSync('./schema.json', 'utf8'));
const track06 = JSON.parse(fs.readFileSync('./examples/track06.json', 'utf8'));
const track09 = JSON.parse(fs.readFileSync('./examples/track09.json', 'utf8'));

describe('track schema', () => {
  test('validates track06.json', () => {
    const validate = ajv.compile(schema);
    validate(track06);
    expect(validate.errors).toBe(null);
  });
  test('validates track09.json', () => {
    const validate = ajv.compile(schema);
    validate(track09);
    expect(validate.errors).toBe(null);
  });
});
