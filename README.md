# Roller Coaster Challenge Schema

This repository contains JSON Schemas to define tracks, challenges and solutions for Roller Coaster Challenge.

## License

[Roller Coaster Challenge](https://www.thinkfun.com/products/roller-coaster-challenge/) is a Trademark of ThinkFun and has been invented by Oli Morris. This 100% fan project is not affiliated with ThinkFun nor Oli Morris. No assets with their copyright have been used.

<p xmlns:dct="http://purl.org/dc/terms/" xmlns:vcard="http://www.w3.org/2001/vcard-rdf/3.0#">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="http://i.creativecommons.org/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://chrpaul.de">
    <span property="dct:title">Christian Paul</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Roller Coaster Challenge Schema</span>.
This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="DE" about="https://chrpaul.de">
  Germany</span>.
</p>
